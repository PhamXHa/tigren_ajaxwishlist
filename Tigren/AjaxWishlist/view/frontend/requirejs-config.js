/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            addWishListAjax: 'Tigren_AjaxWishlist/js/wishlist'
        }
    }
};
