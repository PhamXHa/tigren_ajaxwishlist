define([
    'jquery',
    'Magento_Ui/js/modal/modal',
    'mage/url'
], function ($, modal,urlBuilder) {
    'use strict';
    $.widget('tigren.addWishListAjax', {
        options: {
            method: 'post',
            action: 'checkproduct',
            product: null,
            wishlistBtnSelector: '[data-action="add-to-wishlist"]',
            btnCancelSelector: '.ajaxwishlist_btn_cancel',
            btnAddtoWishlist: '#add-to-wishlist-ajax',
            inputProducId: '[name="product"]',
            selectedOptions: '.super-attribute-select'
        },
        _create: function () {
            this._bind();
            this._popupSuccess();
        },

        _bind: function () {
            var self = this;
            $('body').on('click', self.options.btnCancelSelector, function (e) {
                e.preventDefault();
                e.stopPropagation();
                self.closePopup();
            });
            $('body').on('click', self.options.wishlistBtnSelector, function (e) {
                e.preventDefault();
                e.stopPropagation();
                var product = JSON.parse($(this).attr('data-post'));
                var productId = product.data.product;
                self._ajaxSubmit(productId);
            });
            $('body').on('click', self.options.btnAddtoWishlist, function (e) {
                e.preventDefault();
                e.stopPropagation();
                self._ajaxAddToWishlist($('form#product_addtocart_form'));
            });

        },

        _ajaxSubmit: function (productId) {
            var self = this;
            var url = urlBuilder.build("ajaxwishlist/index/add");
            $.ajax({
                url: url,
                type: self.options.method,
                dataType: 'json',
                data: {product: productId},
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                success: function (res) {
                    $('body').trigger('processStop');
                    if (typeof (res.data.login) != 'undefined' && res.data.login == false) {
                        alert("You must be logged in to add to wishlist.");
                        window.location.href = urlBuilder.build("customer/account/login");
                        return;
                    }
                    $("#modal-overlay #tigren-wishlist-product-information").html(res.data.html);
                    $("#modal-overlay #tigren-wishlist-mess").text('');
                    $("#modal-overlay").modal("openModal");
                    $("#modal-overlay").trigger('contentUpdated');
                }
            });
        },
        _popupSuccess: function () {
            var options = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                buttons: [{
                    text: $.mage.__('Close'),
                    class: '',
                    click: function () {
                        this.closeModal();
                    }
                }]
            };
            var popup = modal(options, $('#modal-overlay'));
        },
        _ajaxAddToWishlist: function (form) {
            var self = this;
            var data = form.serialize();
            var url = urlBuilder.build("ajaxwishlist/index/addtowishlist");
            $.ajax({
                url: url,
                type: self.options.method,
                dataType: 'json',
                data: data,
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                success: function (res) {
                    $('body').trigger('processStop');
                    if (typeof (res.data.success) != 'undefined' && res.data.success == true) {
                        $("#modal-overlay #tigren-wishlist-product-information").html(res.data.html);
                        $("#modal-overlay #tigren-wishlist-mess").text('Add to wishlist success.');
                        $("#modal-overlay").modal("openModal");
                        $("#modal-overlay").trigger('contentUpdated');
                    }
                    if (typeof (res.data.error) != 'undefined' && res.data.error == true) {
                        $("#modal-overlay #tigren-wishlist-product-information").html(res.data.html);
                        $("#modal-overlay #tigren-wishlist-mess").text('Add to wishlist error.');
                        $("#modal-overlay").modal("openModal");
                        $("#modal-overlay").trigger('contentUpdated');
                    }
                }
            });
        },
        closePopup: function () {
            $("#modal-overlay").modal("closeModal");
        }
    });
    return $.tigren.addWishListAjax;
});