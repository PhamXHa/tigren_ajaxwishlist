<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\AjaxWishlist\Controller\Index;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Product;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\LayoutFactory;


/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Add extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $_customerSession;
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var
     */
    protected $wishlistProvider;
    /**
     * @var
     */
    protected $_coreRegistry;
    /**
     * @var
     */
    protected $resultPageFactory;
    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;
    /**
     * @var
     */
    protected $resultFactory;
    /**
     * @var Product
     */
    protected $helperProduct;
    protected $productMetadata;

    /**
     * Add constructor.
     * @param Session $customerSession
     * @param JsonFactory $resultJsonFactory
     * @param ProductRepositoryInterface $productRepository
     * @param LayoutFactory $layoutFactory
     * @param Product $helperProduct
     * @param Action\Context $context
     */
    public function __construct(
        Session $customerSession,
        JsonFactory $resultJsonFactory,
        ProductRepositoryInterface $productRepository,
        LayoutFactory $layoutFactory,
        Product $helperProduct,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        Action\Context $context
    )
    {
        $this->_customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        $this->layoutFactory = $layoutFactory;
        $this->helperProduct = $helperProduct;
        $this->productMetadata = $productMetadata;
        parent::__construct($context);
    }

    /**
     * Adding new item
     *
     * @return Redirect
     * @throws NotFoundException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function execute()
    {
        $session = $this->_customerSession;
        $resultJson = $this->resultJsonFactory->create();
        if (!$session->isLoggedIn()) {
            $data['login'] = false;
            return $resultJson->setData(['data' => $data]);
        }
        $data['login'] = true;
        $requestParams = $this->getRequest()->getParams();

        if ($session->getBeforeWishlistRequest()) {
            $requestParams = $session->getBeforeWishlistRequest();
            $session->unsBeforeWishlistRequest();
        }

        $productId = isset($requestParams['product']) ? (int)$requestParams['product'] : null;
        $product = $this->productRepository->getById($productId);

        $this->helperProduct->initProduct($productId, 'ajaxwishlist_index_add');
        $layout = $this->layoutFactory->create();
        $version = $this->productMetadata->getVersion();
        if(version_compare($version,'2.3.1', '>')){
                $layout->getUpdate()->load(['ajaxwishlist_index_add']);
            }
        else{
                $layout->getUpdate()->load(['ajaxwishlist_index_add_version_not_update']);
            }
        $layout->generateXml();
        $layout->generateElements();
        $output = $layout->getOutput();
        $html = $output;
        $data['html'] = $html;
        return $resultJson->setData(['data' => $data]);
    }
}
