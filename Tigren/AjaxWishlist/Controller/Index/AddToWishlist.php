<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Tigren\AjaxWishlist\Controller\Index;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Wishlist\Helper\Data;


/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AddToWishlist extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Session
     */
    protected $_customerSession;
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var WishlistProviderInterface
     */
    protected $wishlistProvider;
    /**
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;
    /**
     * @var ResultFactory
     */
    protected $resultFactory;
    /**
     * @var Product
     */
    protected $helperProduct;
    /**
     * @var ProductFactory
     */
    protected $productFactory;
    protected $productMetadata;

    /**
     * AddToWishlist constructor.
     * @param Session $customerSession
     * @param JsonFactory $resultJsonFactory
     * @param WishlistProviderInterface $wishlistProvider
     * @param ProductRepositoryInterface $productRepository
     * @param Registry $registry
     * @param LayoutFactory $layoutFactory
     * @param ResultFactory $resultFactory
     * @param PageFactory $resultPageFactory
     * @param Product $helperProduct
     * @param ProductFactory $productFactory
     * @param Action\Context $context
     */
    public function __construct(
        Session $customerSession,
        JsonFactory $resultJsonFactory,
        WishlistProviderInterface $wishlistProvider,
        ProductRepositoryInterface $productRepository,
        Registry $registry,
        LayoutFactory $layoutFactory,
        ResultFactory $resultFactory,
        PageFactory $resultPageFactory,
        Product $helperProduct,
        ProductFactory $productFactory,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        Action\Context $context
    )
    {
        $this->_customerSession = $customerSession;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;
        $this->wishlistProvider = $wishlistProvider;
        $this->_coreRegistry = $registry;
        $this->resultPageFactory = $resultPageFactory;
        $this->layoutFactory = $layoutFactory;
        $this->resultFactory = $resultFactory;
        $this->helperProduct = $helperProduct;
        $this->productFactory = $productFactory;
        $this->productMetadata = $productMetadata;
        parent::__construct($context);
    }

    /**
     * Adding new item
     *
     * @return Redirect
     * @throws NotFoundException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $session = $this->_customerSession;
        $requestParams = $this->getRequest()->getParams();
        $productId = isset($requestParams['product']) ? (int)$requestParams['product'] : null;
        $_product = $this->productFactory->create();
        if (isset($requestParams['super_attribute'])) {
            $arayAttribute = $requestParams['super_attribute'];
        }
        $htmlOptions = '';
        if (isset($arayAttribute) && $arayAttribute) {
            foreach ($arayAttribute as $key => $value) {
                $isAttributeExist = $_product->getResource()->getAttribute($key);
                $labelAttribute = $isAttributeExist->getFrontend()->getLabel($_product);
                $optionText = '';
                if ($isAttributeExist && $isAttributeExist->usesSource()) {
                    $optionText = $isAttributeExist->getSource()->getOptionText($value);
                }
                if ($optionText) {
                    $htmlOptions .= "<div class='attribute-infomation' style='text-align:center;'>" . "<span class='label-attribute' style=' color:red; font-size:20px'>" . $labelAttribute . "</span> : <span>" . $optionText . "</span></div>";
                }
            }
        }
        $product = $this->productRepository->getById($productId);
        $wishlist = $this->wishlistProvider->getWishlist();
        if (!$wishlist) {
            throw new NotFoundException(__('Page not found.'));
        }

        try {
            $product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $e) {
            $product = null;
        }
        try {
            $buyRequest = new DataObject($requestParams);

            $result = $wishlist->addNewItem($product, $buyRequest);
            if (is_string($result)) {
                throw new LocalizedException(__($result));
            }
            if ($wishlist->isObjectNew()) {
                $wishlist->save();
            }
            $this->_eventManager->dispatch(
                'wishlist_add_product',
                ['wishlist' => $wishlist, 'product' => $product, 'item' => $result]
            );

            $referer = $session->getBeforeWishlistUrl();
            if ($referer) {
                $session->setBeforeWishlistUrl(null);
            } else {
                $referer = $this->_redirect->getRefererUrl();
            }
            $this->_objectManager->get(Data::class)->calculate();
            $data['success'] = true;
            $this->helperProduct->initProduct($productId, 'ajaxwishlist_index_add_success');
            $layout = $this->layoutFactory->create();
            $version = $this->productMetadata->getVersion();
            if(version_compare($version,'2.3.1', '>')){
                $layout->getUpdate()->load(['ajaxwishlist_index_add_success']);
            }
            else{
                $layout->getUpdate()->load(['ajaxwishlist_index_add_success_version_not_update']);
            }
            $layout->createBlock('Tigren\AjaxWishlist\Block\Product\ProductOpitons')
                ->setTemplate('Tigren_AjaxWishlist::wishlist/test.phtml')
                ->toHtml();
            $layout->generateXml();
            $layout->generateElements();
            $output = $layout->getOutput();
            $html = $htmlOptions ? $output . "<h2>Product Options add to wishlist.</h2>" . $htmlOptions : $output;
            $data['html'] = $html;
            return $resultJson->setData(['data' => $data]);
        } catch (LocalizedException $e) {
            $data['error'] = true;
        } catch (Exception $e) {
            $data['error'] = true;
        }
        $data['error'] = false;
        $data['success'] = false;
        return $resultJson->setData(['data' => $data]);
    }
}
