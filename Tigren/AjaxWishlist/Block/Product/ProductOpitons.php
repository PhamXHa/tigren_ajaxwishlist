<?php

namespace Tigren\AjaxWishlist\Block\Product;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class PopupProduct
 * @package Tigren\AjaxWishlist\Block\Wishlist
 */
class ProductOpitons extends Template
{
    /**
     * PopupProduct constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }
    public function getTemplate(){
        return 'Tigren_AjaxWishlist::wishlist/test.phtml';
    }

}