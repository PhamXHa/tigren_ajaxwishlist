<?php

namespace Tigren\AjaxWishlist\Block\Wishlist;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class PopupProduct
 * @package Tigren\AjaxWishlist\Block\Wishlist
 */
class PopupProduct extends Template
{
    /**
     * PopupProduct constructor.
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

}